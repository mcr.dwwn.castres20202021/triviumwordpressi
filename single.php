<?php get_header(); ?>

<div class="main">
    <h1 class="title"><?php the_title(); ?></h1>

        <div class="miniature">
        Miniature : <br/>
        <img class="image" src="<?php echo get_field('image') ?>" alt="<?php the_title(); ?>">
        </div>


    <h2>Details : </h2>
        <div class="para2"><p><?php the_content(); ?></p></div>
        <img class="image2" src="<?php echo get_field('image') ?>" alt="<?php the_title(); ?>">
        <ul>
            <li>Taille : <?php echo get_field('toise'); ?></li>
        </ul>
        <h3>Mange ? : </h3>
        <?php $mange = get_field('mange'); ?>

        <ul>
        <?php foreach ($mange as $compositeur) { ?>
            <a href="<?php echo $compositeur->guid; ?>">
            <li><?php echo $compositeur->post_title; ?></li>
            </a>
        <?php } ?>
           
        </ul>
