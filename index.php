<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bestiaire</title>

    

	<div class="title">
    <img src="https://images.theconversation.com/files/319375/original/file-20200309-118956-1cqvm6j.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=900.0&fit=crop" alt="dog" width="500" height="150">
        <?php get_header(); ?>
    </div>


</head>

<body>
    <main>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="div_sep">
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <ul>
                    <li>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </li>
                </ul>
        </div>
        <?php endwhile; endif;  ?>
        
    </main>
</body>
    <?php get_sidebar(); // Affichage Search, Archives, Category, meta .. ?> 

    <?php get_footer(); // Affichage footer ?>

</html>

